<?php

namespace Nitra\SeoBundle\Controller\Seo;

use Nitra\StoreBundle\Controller\NitraController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Nitra\StoreBundle\Lib\Globals;

class SeoController extends NitraController
{
    protected $uri;

    protected $reflections;

    protected $placeholders = array(
        'category' => '\Nitra\ProductBundle\Document\Category',
        'brand'    => '\Nitra\ProductBundle\Document\Brand',
        'product'  => '\Nitra\ProductBundle\Document\Product',
        'model'    => '\Nitra\ProductBundle\Document\Model',
    );

    /**
     * @Template("NitraSeoBundle:Seo:seoInfo.html.twig")
     *
     * @param string $type
     * @param mixed  $objSeoInfo
     * @param mixed  $object
     * @param string $requestUri
     * @param string $page
     *
     * @return array Template context
     */
    public function seoInfoAction($type, $objSeoInfo = null, $object = null, $requestUri = '', $page = null)
    {
        $this->uri = $this->processUri($requestUri);

        $cachedSeo = $this->getCachedSeo();

        // если страница уже посещалась, то берем сео тексты из кеша
        if (array_key_exists($requestUri, $cachedSeo)) {
            $seo = $cachedSeo[$requestUri];
        } else {
            // Ищем  seo информацию по Uri
            $seoInfo = $this->getDocumentManager()->getRepository('NitraSeoBundle:Seo')->findOneByKey($this->uri);

            // Ищем шаблон seo информацию по page
            $seoTemplateInfo = $this->getSeoTemplate($object, $page);

            if (!$objSeoInfo) {
                $objSeoInfo = ($object && in_array('getSeoInfo', get_class_methods($object))) ? $object->getSeoInfo() : false;
            }
            $store = Globals::getStore();

            // Если есть  seo информацию по объекту (товару, категории ...), то берем ее
            if ($objSeoInfo && ($objSeoInfo->getMetaTitle() || $objSeoInfo->getMetaDescription() || $objSeoInfo->getMetaKeywords() || $objSeoInfo->getDescription())) {
                $seo['metaTitle']       = $objSeoInfo->getMetaTitle()       ? $objSeoInfo->getMetaTitle()       : $store['metaTitle'];
                $seo['metaDescription'] = $objSeoInfo->getMetaDescription() ? $objSeoInfo->getMetaDescription() : $store['metaDescription'];
                $seo['metaKeywords']    = $objSeoInfo->getMetaKeywords()    ? $objSeoInfo->getMetaKeywords()    : $store['metaKeywords'];
                $seo['description']     = $objSeoInfo->getDescription()     ? $objSeoInfo->getDescription()     : $store['description'];
            // Если есть  seo информацию по Uri, то берем ее
            } elseif ($seoInfo) {
                $seo['metaTitle']       = $seoInfo->getMetaTitle()          ? $seoInfo->getMetaTitle()          : $store['metaTitle'];
                $seo['metaDescription'] = $seoInfo->getMetaDescription()    ? $seoInfo->getMetaDescription()    : $store['metaDescription'];
                $seo['metaKeywords']    = $seoInfo->getMetaKeywords()       ? $seoInfo->getMetaKeywords()       : $store['metaKeywords'];
                $seo['description']     = $seoInfo->getDescription()        ? $seoInfo->getDescription()        : $store['description'];
                $this->pushRefKey($seoInfo);
            // if page is category and has found seo template
            } elseif ($page == 'category_page' && $seoTemplateInfo) {
                $seo = $seoTemplateInfo;
            // Если есть шаблон seo - обрабатываем
            } elseif ($seoTemplateInfo) {
                $seo = $seoTemplateInfo;
            // Иначе берм сео из магазина
            } else {
                $seo['metaTitle']       = $store['metaTitle'];
                $seo['metaDescription'] = $store['metaDescription'];
                $seo['metaKeywords']    = $store['metaKeywords'];
                $seo['description']     = $store['description'];
                // для очистки кеша при редактировании магазина
                $this->pushRefKey('Store', $store['id']);
            }
            if (is_object($object)) {
                $this->pushRefKey($object);
                // для очистки кеша при добавлении seo шаблона, если передан объект
                $this->pushRefKey('SeoTemplate');
            }
            $seo['source'] = $this->reflections;
            $cachedSeo[$requestUri] = $seo;
            $this->getCache()->save($this->getCacheKey(), $cachedSeo, 60 * 60);
        }

        return array(
            'type'  => $type,
            'seo'   => $seo,
        );
    }

    /**
     * @param string $uri
     *
     * @return string
     */
    protected function processUri($uri)
    {
        $routerContext = $this->get('router')->getContext();
        $decoded       = urldecode(str_replace($routerContext->getBaseUrl(), '', $uri));

        if ($this->container->hasParameter('locales') && ($locales = $this->container->getParameter('locales'))) {
            return preg_replace('/^\/(' . implode('|', $locales) . ')/', '', $decoded);
        } else {
            return $decoded;
        }
    }

    /**
     * Push reference key
     *
     * @param object        $obj
     * @param null|string   $id
     */
    protected function pushRefKey($obj, $id = null)
    {
        $sName = null;
        if (is_object($obj)) {
            $reflection = new \ReflectionClass($obj);
            $sName = $reflection->getShortName();
        } elseif (is_string($obj)) {
            $sName = $obj;
        }
        $uid = (($id) ? $id : (is_object($obj) ? $obj->getId() : null));
        if ($sName && $uid) {
            $this->reflections[] = $sName . '_' . $uid;
        } elseif ($sName) {
            $this->reflections[] = $sName;
        }
    }

    /**
     * Map cache key
     *
     * @return string
     */
    protected function getCacheKey()
    {
        return $this->mapCacheKey('seo_info_by_uris');
    }

    /**
     * @return array
     */
    protected function getCachedSeo()
    {
        $key    = $this->getCacheKey();
        $cache  = $this->getCache();
        if ($cache->contains($key)) {
            return $cache->fetch($key);
        } else {
            return array();
        }
    }

    /**
     * Get seo template
     *
     * @param object $object
     * @param string $page
     *
     * @return array|bool
     */
    protected function getSeoTemplate($object, $page)
    {
        if (!$object || !$page) {
            return false;
        }
        $dm = $this->getDocumentManager();

        if ((method_exists($object, 'getCategory') && $object->getCategory()) || ($object instanceof \Nitra\ProductBundle\Document\Category)) {
            if ($object instanceof \Nitra\ProductBundle\Document\Category) {
                $seoInfo    = $this->getCategorySeoTemplate($object, $page);
            } else {
                $categoryId = $object->getCategory()->getId();
                $seoInfo    = $dm->getRepository('NitraSeoBundle:SeoTemplate')->findOneBy(array(
                    'category.$id'  => new \MongoId($categoryId),
                    'location.$id'  => $page,
                ));
            }

            // если не найдено с категорией, поиск по шаблонам, у которых не указана категория
            if (!$seoInfo) {
                $seoInfo = $dm->getRepository('NitraSeoBundle:SeoTemplate')->findOneBy(array(
                    'category'      => array(
                        '$exists'       => false,
                    ),
                    'location.$id'  => $page,
                ));
            }
        } else {
            $seoInfo = $dm->getRepository('NitraSeoBundle:SeoTemplate')->findOneBy(array('location.$id' => $page));
        }

        if ($seoInfo) {
            $seo        = array();

            $seo['description']     = $seoInfo->getDescription();
            $seo['metaTitle']       = $seoInfo->getMetaTitle();
            $seo['metaDescription'] = $seoInfo->getMetaDescription();
            $seo['metaKeywords']    = $seoInfo->getMetaKeywords();

            $methods = $this->getMethods($object, $seo);
            $this->replace($object, $methods, $seo);

            $this->pushRefKey($seoInfo);

            $seo['isLigament']      = true;

            return $seo;
        } else {
            return false;
        }
    }

    /**
     * Поиск сео шаблона для категории (с учетом родителей)
     *
     * @param \Nitra\ProductBundle\Document\Category $category
     *
     * @return \Nitra\SeoBundle\Document\SeoTemplate|null
     */
    protected function getCategorySeoTemplate($category, $page)
    {
        $seoInfo = $this->getDocumentManager()->getRepository('NitraSeoBundle:SeoTemplate')->findOneBy(array(
            'category.$id'  => new \MongoId($category->getId()),
            'location.$id'  => $page,
            'ligaments'     => $this->getLigamentsQuery(),
        ));

        if (!$seoInfo && $category->getParent()) {
            $this->pushRefKey($category->getParent());
            return $this->getCategorySeoTemplate($category->getParent(), $page);
        } else {
            return $seoInfo;
        }
    }

    /**
     * Get query for find seo template by selected filters
     *
     * @return array
     */
    protected function getLigamentsQuery()
    {
        $queryParameters = $this->getQueryParameters();

        // define query array
        $query = array();
        // iterate keys of query parameters
        foreach (array_keys($queryParameters) as $parameter) {
            // check key of is special
            if (in_array($parameter, array('page', 'limit', 'view', 'sort'))) {
                continue;
            }

            // add elemMatch to query
            $query[] = array(
                '$elemMatch' => array(
                    'data' => new \MongoRegex('/"placeholder":"' . $parameter . '"/'),
                ),
            );
        }

        return array(
            '$elemMatch' => array(
                // each of elemMatch must match
                '$all'  => $query,
                // and size of ligaments items query
                '$size' => count($query),
            ),
        );
    }

    /**
     * @param string $class
     *
     * @return \Nitra\SeoBundle\Document\DocsSeoTemplate|null
     */
    protected function getDocsSeo($class)
    {
        return $this->getDocumentManager()->getRepository('\Nitra\SeoBundle\Document\DocsSeoTemplate')
            ->findOneByClass($class);
    }

    /**
     * @param object $object
     * @param array $seo
     *
     * @return array
     */
    protected function getMethods($object, $seo)
    {
        $methods = array();
        foreach ($seo as $s) {
            $found = array();
            preg_match_all('/{([\w-]+)\|*([\w-]{0,1})}/', $s, $found);
            foreach ($found[0] as $key => $f) {
                $getter = trim($found[1][$key]);
                if (array_key_exists($getter, $this->placeholders) && ($object instanceof $this->placeholders[$getter])) {
                    $getter = 'name';
                }
                $case = trim($found[2][$key]);
                $methods[$f] = array(
                    'method' => $getter,
                    'case'   => $case,
                );
            }
        }

        return $methods;
    }

    /**
     * Get replace value from query parameters
     *
     * @param string $key
     * @param string $value
     *
     * @return string
     */
    protected function getReplaceValue($key, $value)
    {
        $results = array();
        switch ($key) {
            case 'brand':
                if ($result = $this->getSelectedFilterBrands($value)) {
                    $results[] = $result;
                }
                break;
            case 'color':
                if ($result = $this->getSelectedFilterColors($value)) {
                    $results[] = $result;
                }
                break;
            case 'season':
                if ($result = $this->getSelectedFilterSeasons($value)) {
                    $results[] = $result;
                }
                break;
            default:
                if ($result = $this->getSelectedParameter($key, $value)) {
                    $results[] = $result;
                }
                break;
        }

        return implode(' ', $results);
    }

    /**
     * Parse uri and gert query parameters
     *
     * @return array
     */
    protected function getQueryParameters()
    {
        // parse link
        $parsed = parse_url($this->uri);

        // if parsed url don contains query parameters
        if (!isset($parsed['query'])) {
            // define him as empty
            $parsed['query'] = '';
        }

        // define query parameters array
        $queryParameters = array();
        // parse string query
        parse_str($parsed['query'], $queryParameters);

        return $queryParameters;
    }

    /**
     * Replace reserved string of value from object
     *
     * @param object $object
     * @param array $methods
     *
     * @param array $seo
     */
    protected function replace($object, $methods, &$seo)
    {
        $queryParameters = $this->getQueryParameters();
        $replaces = array();
        foreach ($methods as $search => $method) {
            $getter = 'get' . ucfirst($method['method']);
            if (method_exists($object, $getter)) {
                $replaces[$search] = (string) $object->$getter();
            } elseif (array_key_exists($method['method'], $queryParameters)) {
                $replaces[$search] = $this->getReplaceValue($method['method'], $queryParameters[$method['method']]);
            }
            $replaces[$search] = $this->caser($replaces[$search], $method['case']);
        }

        foreach ($seo as $seoKey => $subject) {
            $seo[$seoKey] = str_replace(array_keys($replaces), array_values($replaces), $subject);
        }
    }

    /**
     * change registry of string
     *
     * @param string $str
     * @param string $case
     *
     * @return string
     */
    protected function caser($str, $case)
    {
        $mode = null;
        switch (mb_strtolower(trim($case), 'UTF-8')) {
            case 'u':
                $mode = MB_CASE_UPPER;
                break;
            case 'c':
                $mode = MB_CASE_TITLE;
                break;
            case 'l':
                $mode = MB_CASE_LOWER;
                break;
        }

        return !is_null($mode)
            ? mb_convert_case($str, $mode, 'UTF-8')
            : $str;
    }

    /**
     * Get selected filter brands
     *
     * @param string $selectedBrands
     *
     * @return string
     */
    protected function getSelectedFilterBrands($selectedBrands)
    {
        $brands = $this->getDocumentManager()->createQueryBuilder('NitraProductBundle:Brand')
            ->field('alias_en')->in(explode(',', $selectedBrands))
            ->getQuery()->execute();

        $names = array();
        foreach ($brands as $brand) {
            $names[] = $brand->getName();
        }

        return implode(", ", $names);
    }

    /**
     * Get selected filter colors
     *
     * @param string $selectedColors
     *
     * @return string|null
     */
    protected function getSelectedFilterColors($selectedColors)
    {
        if (!class_exists('\Nitra\ProductBundle\Document\Color')) {
            return null;
        }
        $colors = $this->getDocumentManager()->createQueryBuilder('NitraProductBundle:Color')
            ->field('alias_en')->in(explode(',', $selectedColors))
            ->getQuery()->execute();

        $names = array();
        foreach ($colors as $color) {
            $names[] = $color->getName();
        }

        return implode(", ", $names);
    }

    /**
     * Get selected filter seasons
     *
     * @param array  $selectedSeasons
     *
     * @return string|null
     */
    protected function getSelectedFilterSeasons($selectedSeasons)
    {
        if (!class_exists('\Nitra\ProductBundle\Document\Season')) {
            return null;
        }
        $seasons = $this->getDocumentManager()->createQueryBuilder('NitraProductBundle:Season')
            ->field('alias_en')->in(explode(',', $selectedSeasons))
            ->getQuery()->execute();

        $names = array();
        foreach ($seasons as $season) {
            $names[] = $season->getName();
        }

        return implode(", ", $names);
    }

    /**
     * Get selected parameter values
     *
     * @param string $parameterAlias
     * @param string $selectedValues
     *
     * @return string|null
     */
    protected function getSelectedParameter($parameterAlias, $selectedValues)
    {
        $result = array();
        $parameter = $this->getDocumentManager()->createQueryBuilder('NitraProductBundle:Parameter')
            ->field('alias_en')->equals($parameterAlias)
            ->getQuery()->getSingleResult();
        if (!$parameter) {
            return;
        }

        $selected = explode(',', $selectedValues);
        foreach ($parameter->getParameterValues() as $value) {
            if (in_array($value->getAliasEn(), $selected)) {
                $result[] = $value->getName();
            }
        }

        return implode(', ', $result);
    }
}