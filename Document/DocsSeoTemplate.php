<?php

namespace Nitra\SeoBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Документ для seo шаблона фильтруемых документов
 * (документов, которые участвуют в фильтрации товаров, бренда к примеру или цвета)
 * 
 * @author Paramonov Igor <i.paramonov@nitralabs.com>
 * @ODM\Document
 */
class DocsSeoTemplate
{
    /**
     * @ODM\Id(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ODM\String
     */
    private $class;
    
    /**
     * @Gedmo\Translatable
     * @ODM\String
     */
    private $description;

    /**
     * @Gedmo\Translatable
     * @ODM\String
     */
    private $metaTitle;

    /**
     * @Gedmo\Translatable
     * @ODM\String
     */
    private $metaDescription;

    /**
     * @Gedmo\Translatable
     * @ODM\String
     */
    private $metaKeywords;

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set class
     *
     * @param string $class
     * @return self
     */
    public function setClass($class)
    {
        $this->class = $class;
        return $this;
    }

    /**
     * Get class
     *
     * @return string $class
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Get description
     *
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set metaTitle
     *
     * @param string $metaTitle
     * @return self
     */
    public function setMetaTitle($metaTitle)
    {
        $this->metaTitle = $metaTitle;
        return $this;
    }

    /**
     * Get metaTitle
     *
     * @return string $metaTitle
     */
    public function getMetaTitle()
    {
        return $this->metaTitle;
    }

    /**
     * Set metaDescription
     *
     * @param string $metaDescription
     * @return self
     */
    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription;
        return $this;
    }

    /**
     * Get metaDescription
     *
     * @return string $metaDescription
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * Set metaKeywords
     *
     * @param string $metaKeywords
     * @return self
     */
    public function setMetaKeywords($metaKeywords)
    {
        $this->metaKeywords = $metaKeywords;
        return $this;
    }

    /**
     * Get metaKeywords
     *
     * @return string $metaKeywords
     */
    public function getMetaKeywords()
    {
        return $this->metaKeywords;
    }
}