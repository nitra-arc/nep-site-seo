<?php

namespace Nitra\SeoBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ODM\Document
 */
class SeoTemplate
{
    use \Gedmo\Blameable\Traits\BlameableDocument;

    /**
     * @var string Identifier
     * @ODM\Id
     */
    protected $id;

    /**
     * @var array Location of seo template
     * @ODM\Hash
     */
    protected $location;

    /**
     * @var \Nitra\ProductBundle\Document\Category Category of seo template
     * @ODM\ReferenceOne(targetDocument="Nitra\ProductBundle\Document\Category")
     */
    protected $category;

    /**
     * @var string Seo description
     * @ODM\String
     * @Gedmo\Translatable
     */
    protected $description;

    /**
     * @var string Meta title
     * @ODM\String
     * @Gedmo\Translatable
     */
    protected $metaTitle;

    /**
     * @var string Meta description
     * @ODM\String
     * @Gedmo\Translatable
     */
    protected $metaDescription;

    /**
     * @var string Meta keywords
     * @ODM\String
     * @Gedmo\Translatable
     */
    protected $metaKeywords;

    /**
     * @var array Filter ligaments
     * @ODM\Collection
     */
    protected $ligaments;

    /**
     * Get id
     * @return string $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get location
     * @return array $location
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Get description
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Get metaTitle
     * @return string $metaTitle
     */
    public function getMetaTitle()
    {
        return $this->metaTitle;
    }

    /**
     * Get metaDescription
     * @return string $metaDescription
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * Get metaKeywords
     * @return string $metaKeywords
     */
    public function getMetaKeywords()
    {
        return $this->metaKeywords;
    }

    /**
     * Get category
     * @return \Nitra\ProductBundle\Document\Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Get ligaments
     * @return array
     */
    public function getLigaments()
    {
        $ligaments = $this->ligaments ?: array(
            array(),
        );
        foreach ($ligaments as &$ligament) {
            $multisort = array();
            foreach ($ligament as $key => $value) {
                $multisort[$key] = (int) $value['order'];
            }
            array_multisort($multisort, SORT_ASC, $ligament);
        }

        return $ligaments;
    }
}