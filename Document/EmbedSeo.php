<?php

namespace Nitra\SeoBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @MongoDB\EmbeddedDocument 
 */
class EmbedSeo
{
    /**
     * @MongoDB\Id(strategy="AUTO")
     */
    private $id;
    
    /**
     * @Gedmo\Translatable
     * @MongoDB\Field(type="string")
     */
    protected $description;

    /**
     * @Gedmo\Translatable
     * @MongoDB\String
     * @Assert\Length(max = 255)
     */
    protected $metaTitle;

    /**
     * @Gedmo\Translatable
     * @MongoDB\String
     * @Assert\Length(max = 255)
     */
    protected $metaDescription;

    /**
     * @Gedmo\Translatable
     * @MongoDB\String
     * @Assert\Length(max = 255)
     */
    protected $metaKeywords;

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }
   
    /**
     * Set description
     *
     * @param string $description
     * @return \Seo
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Get description
     *
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set metaTitle
     *
     * @param string $metaTitle
     * @return \Seo
     */
    public function setMetaTitle($metaTitle)
    {
        $this->metaTitle = $metaTitle;
        return $this;
    }

    /**
     * Get metaTitle
     *
     * @return string $metaTitle
     */
    public function getMetaTitle()
    {
        return $this->metaTitle;
    }

    /**
     * Set metaDescription
     *
     * @param string $metaDescription
     * @return \Seo
     */
    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription;
        return $this;
    }

    /**
     * Get metaDescription
     *
     * @return string $metaDescription
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * Set metaKeywords
     *
     * @param string $metaKeywords
     * @return \Seo
     */
    public function setMetaKeywords($metaKeywords)
    {
        $this->metaKeywords = $metaKeywords;
        return $this;
    }

    /**
     * Get metaKeywords
     *
     * @return string $metaKeywords
     */
    public function getMetaKeywords()
    {
        return $this->metaKeywords;
    }
}