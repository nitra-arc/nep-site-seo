# SeoBundle

## Описание
Вывод meta-тегов для страницы, Информация выбирается из БД. БД заполняется из админки.
Данный бандл предназначен для работы (вывода, обработки) с:

* Seo - ключевые слова, метаданные для документов
* EmbedSeo - сео тексты для конкретного товара по конкретной категории
* SeoTemplate - сео тексты для товаров конкретной категории
* DocsSeoTemplate - сео тексты для документов
## SeoController
* seoInfoAction - поиск всевозможных сео шаблонов, обработка данных из кэша

## Подключение
Для подключения данного модуля в проект необходимо:

* composer.json:

```json
{
    ...   
    "require": {
        ...
        "e-commerce-site/seobundle": "dev-master",
        ...
    }
    ...
}
```

* app/AppKernel.php:

```php
<?php

    //...
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    //...
    public function registerBundles()
    {
        //...
        $bundles = array(
            //...
            new Nitra\SeoBundle\NitraSeoBundle(),
            //...
        );
        //...
        return $bundles;
    }
    //...
}
```
* app/config/routing.yml

```php
nitra_seo:
    resource: "@NitraSeoBundle/Controller"
    type: annotation
```